/*Demo*/
document.getElementById("btn-1").addEventListener('click', () => {
        alert("Add more!");
});

let paragraph = document.getElementById("paragraph-1");
let paragraph2 = document.getElementById("paragraph-2");

document.getElementById("btn-2").addEventListener('click', () => {
        paragraph.innerHTML = "I can even do this!";
});

document.getElementById("btn-3").addEventListener('click', () => {
        paragraph2.innerHTML = "Or this!";
        paragraph2.style.color = "red";
        paragraph2.style.fontSize = "50px";
}); /*Demo*/
 
console.log("Hello World!")

console.log("Juvince Bueno");


let num = 10;

console.log(6)
console.log(num);

let name1="Leonardo"

console.log("John")
console.log(name1);


let myVariable;


// console.log(myVariable)

myVariable = "New Initialized Value";
console.log(myVariable);

myVariable = "Another Value";
console.log(myVariable);

myVariable = 7;
console.log(myVariable);


let myVariable2;
myVariable2 = "Initial Value 2";

console.log(myVariable2);



let bestFinalFantasy;
bestFinalFantasy = "Final Fantasy 6"
console.log(bestFinalFantasy);


bestFinalFantasy = "Final Fantasy 7";
console.log(bestFinalFantasy)



// let bestFinalFantasy = "Final Fantasy 10";
// console.log(bestFinalFantasy);


const pi = 3.1416;
console.log(pi);


let favoriteBand = ["Beatles","Parokya ni Edgar", "Eraserheads"];
 console.log(favoriteBand);

 let mePerson = {
        firstName: "Vince",
        lastName: "Bueno",
        isDeveloper: true,
        hasPortfolio: true,
        age: 29
 };
 console.log(mePerson);


 let foundResult = null;
 console.log(foundResult);



let sampleVariable;
console.log(sampleVariable);


 // In strings, spaces count as characters
 let state = "Texas";
 let country = "USA";

 let address = state + ',' + ' ' + country;
 console.log(address);

// Null vs Undefined
/*
        Null is the explicit absence of data/value. This is done to show that a variable contains nothing as opposed to undefined which means that the variable is created but there is not initial value
 */
// use cases of null
// when doing query or search, there of course might be a 0 result.
let foundResult = null;
console.log(foundResult);

// undefined - is a representation that a variable has been created/declared but there is no initial value, so we can't quite say, what the value is, thus it is undefined.
let sampleVariable;
console.log(sampleVariable);

let person1 = {
        name: "Peter",
        age: 42
};

// we use dot notation to select or displya the values of property of an object
console.log(person1.isAdmin);
/*
        For undefined, this is normally caused by developers when creating variables that have no data/value associated/initialized with them.

        undefined, because person1 variable does exist, however, there is no property in the object called isAdmin 

 
        Functions
                - Function in JS, are lines/blocks of code that tell our device/application to perform certain task when called or invoked

                - function are reusable pieces of code with instructions which can be used over and over again just so long as we can call or invoke
 */
let name2 = "Bruno";
console.log(name2);
console.log(name2);
console.log(name2);

// functions are created by declaring the function using the function keyword
function showLeo(){
        console.log("Leo");
        console.log("Leo");
        console.log("Leo");
};

// function invocation/call is when use the function
showLeo();
showLeo();
/*
        Function Declaration is when we create the function
        Function Invocation is when we call/use the function
 */

/*Arguments vs Parameters
        - declared a function using function keyword
        - name - parameters are representation of the argument from an invocation

        - we can use the parameter within the function

        - we can also pass data into the function through our invocation.
        - Data added into the parethesis of a function invocation will be or can be passed into our function, this is called argument

*/
 function greet(name) {
        // console.log("name");
        console.log("Hello!" + name + "," + "how are you doing?")
 };

 greet("Jake");
 greet("Pat");

 /*
        Mini-Activity
        Create a function which is able to display data by passing an argument. The data will be displayed in the console.
                Data is fun!
                JavaScript is fun!
                Reading is fun!

                Name your function as displayMsg()
  */


function displayMsg(PL){
        console.log(PL + " is fun")
}

displayMsg("Data");
displayMsg("Javascript")
displayMsg("Reading")



function displayFullName(firstName, lastName, age){
        console.log(firstName + "" + lastName);
        console.log("You are " + age + " years old.");

};

displayFullName("Jeff", "Bezos", 25);
// Note: Order matters in your argument and parameters
 displayFullName("Cena", "John",32);
 displayFullName(32, "John", "Cena");

function showSum(n1,n2){
        console.log("The sum is: ");
        console.log(n1 + n2);
};

showSum(15, 10);

// You can pass variables as arguments
let sampleNumber1 = 30;
let sampleNumber2 = 25;

showSum(sampleNumber1, sampleNumber2);

/*Variable Scopes

        Variables and constant have scopes.
        meaning if a variable or constant is declared outside of a function, any function succeeding it will be able to have access to that variable.

        However, any variable or constant declared inside a function can only be used within that function or code block

        Function scoped variables and constants can only be accessed within the function where they are declared.
*/

        let sample1 = "This is a sample.";
        const sampleConst = "Sample Constant.";

        function sampleFunc(){
                console.log(sample1);
                console.log(sampleConst);
        };

        sampleFunc();

        function sampleFunc2(){
                console.log(sample1);
        };

        sampleFunc2();

        function sampleFunc3(){
                let sample2 = "This is inside a function.";
                const sampleConst2 = "This is in a function.";

                console.log(sample2);
                console.log(sampleConst2);
        };

        sampleFunc3();

        // function sampleFunc4(){
        //      console.log(sampleConst2);
        // };

        // sampleFunc4();

/* 
        Return keyword allows to return a value
        This also stops the process of the function and any statement after the return should not or will not be processed
        
*/
        function addNum(x,y){
                return x + y;
        };

        // with the return keyword you can return a value and save that value in a variable
        let sampleSum = addNum(2, 3);
        console.log(sampleSum);

        let sampleSum1 = showSum(5, 10);
        console.log(sampleSum1);

        function returnFullName(firstName, middleName, lastName){
                return firstName + middleName + lastName;
        }

        let fullName = returnFullName("Michael", "John", "Smith");
        console.log("My full name is " + fullName);

        function completeDetails(fullName, age, role){
                return {
                        fullName: fullName,
                        age: age,
                        role: role
                }
                // you can return any data type (string, number, boolean, array, object)
                // you can also return a variable created inside a function
        }

        let userName = completeDetails(fullName, 35, "Developer");
        console.log(userName);

